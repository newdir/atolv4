Newdir Atol SDK
===============
## Установка

Проект предполагает установку с использованием composer
<pre><code>composer require newdir/atolv4</pre></code>

## Примеры использования

### 1. Запрос токена

```php
$client = new Newdir\Atol\clients\PostClient();
$tokenService = new Newdir\Atol\services\GetTokenRequest('login', 'password');
$tokenResponse = new Newdir\Atol\services\GetTokenResponse($client->sendRequest($tokenService));
```

### 2. Создание чека

```php
$client = new Newdir\Atol\clients\PostClient();
$receiptPosition = new Newdir\Atol\data_objects\ReceiptPosition('Test product', 10.00, 2, Newdir\Atol\data_objects\ReceiptPosition::TAX_VAT10);

$createDocumentService = (new Newdir\Atol\services\CreateDocumentRequest('token'))
    ->addCustomerEmail('test@test.ru')
    ->addCustomerPhone('79268750000')
    ->addGroupCode('groupCode')
    ->addInn('inn')
    ->addMerchantAddress('paymentAddress')
    ->addOperationType(Newdir\Atol\services\CreateDocumentRequest::OPERATION_TYPE_BUY)
    ->addPaymentType(Newdir\Atol\services\CreateDocumentRequest::PAYMENT_TYPE_ELECTRON)
    ->addSno(Newdir\Atol\services\CreateDocumentRequest::SNO_ESN)
    ->addExternalId('externalId')
    ->addReceiptPosition($receiptPosition);
$createDocumentResponse = new Newdir\Atol\services\CreateDocumentResponse($client->sendRequest($createDocumentService));
```

### 3. Запрос статуса 

```php
$client = new Newdir\Atol\clients\PostClient();
$getStatusService = new Newdir\Atol\services\GetStatusRequest('groupCode', 'uuid', 'token');
$getStatusResponse = new Newdir\Atol\services\GetStatusResponse($client->sendRequest($getStatusService));
```

<?php

/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 30.07.2018
 * Time: 12:42
 */
namespace Newdir\Atol\clients;

use Newdir\Atol\services\BaseServiceRequest;
use Newdir\Atol\SdkException;

class PostClient
{
    /** @var string */
    protected $token;
    /** @var string */
    protected $errorDescription;
    /** @var int */
    protected $errorCode;
    /** @var int */
    protected $connectionTimeout;

    /**
     * @param LoggerInterface $logger
     * @param int $connectionTimeout
     */
    public function __construct($token = null, $connectionTimeout = 30)
    {
        $this->token = $token;
        $this->connectionTimeout = $connectionTimeout;
    }

    /**
     * @inheritdoc
     */
    public function sendRequest(BaseServiceRequest $service)
    {
        $requestParameters = $service->getParameters();
        $token = $this->token;

        if ($token) {
            unset($requestParameters['token']);
        }

        $curl = curl_init($service->getRequestUrl());
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout);

        $json_encode_params = json_encode($requestParameters);

        if(!empty($requestParameters)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $json_encode_params);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json_encode_params),
                ($token) ? 'Token: ' . $token : null)
        );

        $response = curl_exec($curl);

        if(curl_errno($curl)){
            throw new SdkException(curl_error($curl), curl_errno($curl));
        }

        $decodedResponse = json_decode($response);
        if(empty($decodedResponse)){
            throw new SdkExceptions('Atol error. Empty response or not json response');
        }

        return $decodedResponse;
    }
}
<?php

namespace Newdir\Atol\services;

class GetStatusRequest extends BaseServiceRequest{
    
    /** @var string */
    protected $groupCode;
    /** @var string */
    protected $uuId;

    /**
     * @inheritdoc
     */
    public function getRequestUrl() {
        return self::REQUEST_URL.$this->groupCode.'/report/'.$this->uuId;
    }
    
    /**
     * @param string $groupCode
     * @param string $uuId
     * @param string $token
     */
    public function __construct($groupCode, $uuId) {
        $this->groupCode = $groupCode;
        $this->uuId = $uuId;
    }
    
    public function getParameters() {
        return [];
    }
}

<?php

namespace Newdir\Atol\services;

use stdClass;

class CreateDocumentResponse extends BaseServiceResponse {
    
    /** @var string Уникальный идентификатор */
    public $uuid;
    
    /** @var string */
    public $status;
        
    public function __construct(stdClass $response) {
        if(!empty($response->error->code)){
            $this->errorCode = $response->error->code;
            $this->errorDescription = $response->error->text;
            $this->errorId = $response->error->error_id;
            $this->errorType = $response->error->type;
        }
        
        parent::__construct($response);
    }
}

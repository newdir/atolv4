<?php

namespace Newdir\Atol\services;

use stdClass;

class GetTokenResponse extends BaseServiceResponse {
    
    /** @var int */
    public $code;
    /** @var string */
    public $token;
    
    public function __construct(stdClass $response) {
        if($response->error){
            $this->errorCode = $response->error->code;
            $this->errorDescription = $response->error->text;
            $this->errorId = $response->error->id;
            $this->errorType = $response->error->type;
        }
        
        parent::__construct($response);
    }
}
